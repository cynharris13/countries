package com.example.countries.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.countries.model.CountryRepo
import com.example.countries.model.remote.CountryResponse
import com.example.countries.view.countrylist.CountryListScreenState
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * Country view model.
 *
 * @property countryRepo
 * @constructor Create empty Country view model
 */
class CountryViewModel(private val countryRepo: CountryRepo) : ViewModel() {
    private val _state = MutableStateFlow(CountryListScreenState())
    val state: StateFlow<CountryListScreenState> get() = _state

    val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        exception.message?.let { exc ->
            _state.update { it.copy(isLoading = false, error = exc) }
        }
    }

    /**
     * Get country list.
     *
     */
    fun getCountryList() = viewModelScope.launch {
        _state.update { it.copy(isLoading = true) }
        when (val countryResponse = countryRepo.getCountryList()) {
            is CountryResponse.CountrySuccess -> _state.update {
                it.copy(isLoading = false, countries = countryResponse.data)
            }
            is CountryResponse.CountryError -> _state.update {
                it.copy(isLoading = false, error = countryResponse.errorMessage)
            }
        }
    }
}
