package com.example.countries

import android.app.Application
import com.example.countries.model.di.countryModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

/**
 * Countries app.
 *
 * @constructor Create empty Countries app
 */
class CountriesApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@CountriesApp)
            modules(countryModule)
        }
    }
}
