package com.example.countries.view.countrylist

import com.example.countries.model.entity.Country

/**
 * Country list screen state.
 *
 * @property isLoading
 * @property countries
 * @property error
 * @constructor Create empty Country list screen state
 */
data class CountryListScreenState(
    val isLoading: Boolean = false,
    val countries: List<Country> = emptyList(),
    val error: String = "",
)
