package com.example.countries.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import com.example.countries.ui.theme.CountriesTheme
import com.example.countries.view.countrylist.CountryListItem
import com.example.countries.view.countrylist.PaddedText
import com.example.countries.viewmodel.CountryViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : ComponentActivity() {
    private val countryViewModel by viewModel<CountryViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CountriesTheme {
                countryViewModel.getCountryList()
                val state by countryViewModel.state.collectAsState()
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background,
                ) {
                    if (state.isLoading) { CircularProgressIndicator()
                    } else if (state.error.isNotEmpty()) { DisplayError(state.error)
                    } else { LazyColumn { items(state.countries) { CountryListItem(it) } } }
                }
            }
        }
    }
}

@Composable
fun DisplayError(error: String, modifier: Modifier = Modifier) {
    PaddedText(text = error, modifier = modifier.fillMaxWidth())
}
