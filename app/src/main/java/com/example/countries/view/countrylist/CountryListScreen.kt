package com.example.countries.view.countrylist

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.countries.R
import com.example.countries.model.entity.Country

@Composable
fun CountryListItem(country: Country, modifier: Modifier = Modifier) = with(country) {
    Row(modifier = modifier) {
        PaddedText(text = name)
        PaddedText(text = region)
        PaddedText(text = code)
    }
    PaddedText(text = capital)
}

@Composable
fun PaddedText(text: String, modifier: Modifier = Modifier) {
    Text(text = text, modifier = modifier.padding(dimensionResource(id = R.dimen.padding_small)))
}

@Composable
@Preview
fun CountryListPreview() {
    CountryListItem(
        Country(
            capital = "Washington, D.C.",
            name = "United States of America",
            region = "NA",
            code = "US",
        ),
    )
}
