package com.example.countries.model.entity

/**
 * Language.
 *
 * @property code
 * @property iso6392
 * @property name
 * @property nativeName
 * @constructor Create empty Language
 */
data class Language(
    val code: String = "",
    val iso6392: String = "",
    val name: String = "",
    val nativeName: String = "",
)
