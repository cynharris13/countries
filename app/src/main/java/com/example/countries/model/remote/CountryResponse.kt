package com.example.countries.model.remote

import com.example.countries.model.entity.Country

/**
 * Country response.
 *
 * @constructor Create empty Country response
 */
sealed class CountryResponse {

    /**
     * Country success.
     *
     * @property data
     * @constructor Create empty Country success
     */
    class CountrySuccess(val data: List<Country>) : CountryResponse()

    /**
     * Country error.
     *
     * @property errorMessage
     * @constructor Create empty Country error
     */
    class CountryError(val errorMessage: String) : CountryResponse()
}
