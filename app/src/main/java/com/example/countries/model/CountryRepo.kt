package com.example.countries.model

import com.example.countries.model.dto.CountryItem
import com.example.countries.model.entity.Country
import com.example.countries.model.entity.Currency
import com.example.countries.model.entity.Language
import com.example.countries.model.remote.CountryNetwork
import com.example.countries.model.remote.CountryResponse
import io.ktor.client.call.body

/**
 * Country repository.
 *
 * @property network
 * @constructor Create empty Country repo
 */
class CountryRepo(private val network: CountryNetwork) {
    /**
     * Get country list.
     *
     * @return
     */
    suspend fun getCountryList(): CountryResponse {
        val response = network.getCountries()
        return if (response.status.value in MIN_SUCCESS..MAX_SUCCESS) {
            CountryResponse.CountrySuccess(
                response.body<List<CountryItem>>().map { dto ->
                    Country(
                        capital = dto.capital ?: "",
                        code = dto.code ?: "",
                        currency = dto.currencyDTO.let { Currency(it?.code ?: "", it?.name ?: "", it?.symbol ?: "") },
                        demonym = dto.demonym ?: "",
                        flag = dto.flag ?: "",
                        language = dto.language.let {
                            Language(it?.code ?: "", it?.iso6392 ?: "", it?.name ?: "", it?.nativeName ?: "")
                        },
                        name = dto.name ?: "",
                        region = dto.region ?: "",
                    )
                },
            )
        } else { CountryResponse.CountryError(response.status.description) }
    }

    companion object {
        const val MIN_SUCCESS = 200
        const val MAX_SUCCESS = 229
    }
}
