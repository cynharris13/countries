package com.example.countries.model.entity

/**
 * Country.
 *
 * @property capital
 * @property code
 * @property currency
 * @property demonym
 * @property flag
 * @property language
 * @property name
 * @property region
 * @constructor Create empty Country
 */
data class Country(
    val capital: String = "",
    val code: String = "",
    val currency: Currency = Currency(),
    val demonym: String = "",
    val flag: String = "",
    val language: Language = Language(),
    val name: String = "",
    val region: String = "",
)
