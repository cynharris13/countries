package com.example.countries.model.di

import com.example.countries.model.CountryRepo
import com.example.countries.model.remote.CountryNetwork
import com.example.countries.viewmodel.CountryViewModel
import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val countryModule = module {
    single { CountryNetwork(get()) }
    single {
        HttpClient(Android) {
            engine {
                connectTimeout = TIMEOUT
                socketTimeout = TIMEOUT
            }
            install(ContentNegotiation) {
                json(
                    Json {
                        ignoreUnknownKeys = true
                        isLenient = true
                    },
                    contentType = ContentType("text", "plain"),
                )
            }
        }
    }
    single { CountryRepo(get()) }

    viewModel { CountryViewModel(get()) }
}

const val TIMEOUT = 30_000
