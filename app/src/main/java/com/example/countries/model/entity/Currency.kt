package com.example.countries.model.entity

/**
 * Currency.
 *
 * @property code
 * @property name
 * @property symbol
 * @constructor Create empty Currency
 */
data class Currency(
    val code: String = "",
    val name: String = "",
    val symbol: String = "",
)
