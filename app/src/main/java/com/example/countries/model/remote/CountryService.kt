package com.example.countries.model.remote

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse

/**
 * Country service.
 *
 * @constructor Create empty Country service
 */
interface CountryService {
    /**
     * Get countries.
     *
     * @return
     */
    suspend fun getCountries(): HttpResponse
}

/**
 * Country network.
 *
 * @property httpClient
 * @constructor Create empty Country network
 */
class CountryNetwork(private val httpClient: HttpClient) : CountryService {
    /**
     * Get countries.
     *
     * @return
     */
    override suspend fun getCountries(): HttpResponse = httpClient.get(URL)
}
const val URL = "https://gist.githubusercontent.com/peymano-wmt/" +
    "32dcb892b06648910ddd40406e37fdab/raw/db25946fd77c5873b0303b858e861ce724e0dcd0/countries.json"
