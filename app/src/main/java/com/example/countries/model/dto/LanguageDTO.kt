package com.example.countries.model.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LanguageDTO(
    val code: String? = null,
    @SerialName("iso639_2") val iso6392: String? = null,
    val name: String? = null,
    val nativeName: String? = null,
)
