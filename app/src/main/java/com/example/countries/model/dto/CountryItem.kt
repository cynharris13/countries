package com.example.countries.model.dto

import kotlinx.serialization.Serializable

@Serializable
data class CountryItem(
    val capital: String? = null,
    val code: String? = null,
    val currencyDTO: CurrencyDTO? = null,
    val demonym: String? = null,
    val flag: String? = null,
    val language: LanguageDTO? = null,
    val name: String? = null,
    val region: String? = null,
)
