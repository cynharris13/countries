package com.example.countries.model.dto

import kotlinx.serialization.Serializable

@Serializable
data class CurrencyDTO(
    val code: String? = null,
    val name: String? = null,
    val symbol: String? = null,
)
