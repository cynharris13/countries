package com.example.countries.viewmodel

import com.example.countries.CoroutinesTestExtension
import com.example.countries.model.CountryRepo
import com.example.countries.model.entity.Country
import com.example.countries.model.remote.CountryResponse
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
class CountryViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    val repo = mockk<CountryRepo>()
    val countryVM = CountryViewModel(repo)

    @Test
    @DisplayName("Test successful country list fetch")
    fun testGetCountryList() = runTest(extension.dispatcher) {
        // given
        val response = CountryResponse.CountrySuccess(listOf(Country(capital = "lol")))
        coEvery { repo.getCountryList() } coAnswers { response }

        // when
        countryVM.getCountryList()

        // then
        with(countryVM.state.value) {
            Assertions.assertFalse(isLoading)
            Assertions.assertTrue(error.isEmpty())
            Assertions.assertEquals(countries, response.data)
        }
    }

    @Test
    @DisplayName("Test failed country list fetch")
    fun testGetCountryError() = runTest(extension.dispatcher) {
        // given
        val response = CountryResponse.CountryError("no")
        coEvery { repo.getCountryList() } coAnswers { response }

        // when
        countryVM.getCountryList()

        // then
        with(countryVM.state.value) {
            Assertions.assertFalse(isLoading)
            Assertions.assertTrue(countries.isEmpty())
            Assertions.assertEquals(error, response.errorMessage)
        }
    }
}
