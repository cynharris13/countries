package com.example.countries.model

import com.example.countries.CoroutinesTestExtension
import com.example.countries.model.dto.CountryItem
import com.example.countries.model.entity.Country
import com.example.countries.model.remote.CountryNetwork
import com.example.countries.model.remote.CountryResponse
import io.ktor.client.call.body
import io.ktor.http.HttpStatusCode
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
class CountryRepoTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    val service = mockk<CountryNetwork>()
    val repo = CountryRepo(service)

    @Test
    @DisplayName("Tests successfully fetching the country list")
    fun testGetCountries() = runTest(extension.dispatcher) {
        // given
        val dtos = listOf(CountryItem(capital = "lol"))
        coEvery { service.getCountries().status.value } coAnswers { 200 }
        coEvery { service.getCountries().body<List<CountryItem>>() } coAnswers { dtos }
        val expected = CountryResponse.CountrySuccess(listOf(Country(capital = "lol")))

        // when
        val actual = repo.getCountryList()

        // then
        Assertions.assertTrue(actual is CountryResponse.CountrySuccess)
        val actualSuccess = actual as CountryResponse.CountrySuccess
        Assertions.assertEquals(actualSuccess.data, expected.data)
    }

    @Test
    @DisplayName("Tests failing to fetch the list")
    fun testErrorCase() = runTest(extension.dispatcher) {
        // given
        val status = HttpStatusCode(404, "no")
        coEvery { service.getCountries().status } coAnswers { status }
        val expected = CountryResponse.CountryError(status.description)
        // when
        val actual = repo.getCountryList()

        // then
        Assertions.assertTrue(actual is CountryResponse.CountryError)
        val actualError = actual as CountryResponse.CountryError
        Assertions.assertEquals(actualError.errorMessage, expected.errorMessage)
    }
}
